package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in=new Scanner(System.in);
		int discriminante;
		float resultado;
		float resultado2;
		
		System.out.println("La ecuacion de segundo grado es: ax2 + bx + c = 0");
		System.out.println("Introduzca el valor a: ");
		int a = in.nextInt();
		System.out.println("Introduzca el valor b: ");
		int b = in.nextInt();
		System.out.println("Introduzca el valor c: ");
		int c = in.nextInt();
		
		discriminante = (int) (Math.pow(b, 2)-4*a*c);
		
		if(discriminante==0){
			resultado=-b/2*a;
			System.out.println("Solo hay una soluci�n y  es: " + resultado);
		}
		else if(discriminante<0){
			System.out.println("No se puede calcular.");
		}
		else {
			resultado=(float) (-b+Math.sqrt(discriminante)/2*a);
			resultado2=(float) (-b-Math.sqrt(discriminante)/2*a);
			System.out.println("Existen dos soluciones = " + resultado + " "+ resultado2);
		}
		
	}

}
