package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Introduce la hora:");
		int hora=in.nextInt();
		System.out.println("Introduce los minutos: ");
		int minutos = in.nextInt();
		System.out.println("Introduce los segundos: ");
		int segundos = in.nextInt();

		if(hora<0 || hora>23){
			System.out.println("Error al introducir la hora.");
			System.exit(0);
		}
		if (minutos<0 || minutos>59){
			System.out.println("Error al introducir los minutos.");
			System.exit(0);
		}
		if (segundos<0 || segundos>59){
			System.out.println("Error al introducir los segundos.");
			System.exit(0);
		}
		
		if (segundos>0 && segundos<60){
			segundos=segundos+1;
			if (segundos>=60){
				minutos=minutos+1;
				segundos=0;
			}
			if (minutos>=60){
				hora=hora+1;
				minutos=0;
				segundos=0;
				if (hora>=24){
					hora=0;
				}
			}
		}
		System.out.println("La hora un segundo despu�s es: " + hora + ":" + minutos + ":" + segundos);
	
	}

}
