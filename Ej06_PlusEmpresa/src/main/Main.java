package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in= new Scanner(System.in);
		
		System.out.println("Introduce tu estado civil: (1.Soltero 2. Casado 3. Viudo 4. Divorciado)");
		int estado=in.nextInt();
		if(estado<1 || estado>4){
			System.out.println("Error al introducir el estado civil.");
			System.exit(0);
		}
		System.out.println("Introduzca su nivel de estudios: (1. Primario 2. Medio 3. Secundario)");
		int estudios=in.nextInt();
		if(estudios<1 || estudios>3){
			System.out.println("Error al introducir el nivel de estudios.");
			System.exit(0);
		}
		
		if(estado==1){
			if(estudios>=1 && estudios<=3){
			System.out.println("El plus que recibir� ser� de 6 euros.");
			}
		}
		else if(estado==3){
			if(estudios==1 || estudios==3){
				System.out.println("Recibir� un plus de 6 euros");
			}
			else if(estudios==2){
				System.out.println("Recibira un plus de 9 euros.");
			}
		}
		else if(estado==2){
			if(estudios==1){
				System.out.println("Recibir� un plus de 12 euros.");
			}
			else if(estudios==2){
				System.out.println("Recibir� un plus de 15 euros.");
			}
			else if(estudios==3){
				System.out.println("Recibir� un plus de 18 euros.");
			}
		}
		else if(estado==4){
			if(estudios==1){
				System.out.println("Recibir� un plus de 12 euros.");
			}
			else if(estudios==2){
				System.out.println("Recibir� un plus de 14 euros.");
			}
			else if(estudios==3){
				System.out.println("Recibir� un plus de 16 euros.");
			}
		}
	}

}
