package main;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		
		//Introduccion de altura
		System.out.println("Introduce tu altura: ");
		float altura=in.nextFloat();
		if (altura<1f || altura>2f){
			System.out.println("Error al introducir la altura.");
			System.exit(0);
		}
		
		//Introduccion de peso
		System.out.println("Introduce tu peso: ");
		float peso=in.nextFloat();
		if (peso<20 || peso>150){
			System.out.println("Error al introducir el peso.");
			System.exit(0);
		}
		
		//Calculo imc
		float imc = (float) (peso/Math.pow(altura, 2));
		System.out.println("Su imc es: " + imc);
		
		//Escribir el resultado
		if(imc<16){
			System.out.println("Criterio de ingreso en el hospital");
		}
		else if(imc<=17 && imc>=16){
			System.out.println("Infrapeso");
		}
		else if(imc<=18 && imc>17){
			System.out.println("Bajo peso");
		}
		else if(imc<=25 && imc>18){
			System.out.println("Peso normal (saludable)");
		}
		else if(imc<=30 && imc>25){
			System.out.println("Sobrepeso (obesidad grado 1)");
		}
		else if(imc<=35 && imc>30){
			System.out.println("Sobrepeso cr�nico (obesidad grado 2)");
		}
		else if(imc<=40 && imc>35){
			System.out.println("Obesidad prem�rbida (obesidad gardo 3)");
		}
		else if(imc>40){
			System.out.println("Obesidad m�rbida (obesidad gardo 4)");
		}
		
	}
}
